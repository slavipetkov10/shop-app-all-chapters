import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/products.dart';
import './product_item.dart';

// Grid of products loaded in ProductsOverviewScreen
class ProductsGrid extends StatelessWidget {
  final bool showFavs;

  ProductsGrid(this.showFavs);

  @override
  Widget build(BuildContext context) {
    // Setups a direct communication channel behind the scenes, This widgets
    // is listening to changed and will be rebuild
    // In the <> set the type of data to which you want to listen to
    // Setup a listener to the provider which provides an instance of the
    // Products class
    // This Provider.of<Products> is a listener for Products
    final productsData = Provider.of<Products>(context);
    // The list of product items
    final products = showFavs ? productsData.favoriteItems : productsData.items;
    return GridView.builder(
      padding: const EdgeInsets.all(10.0),
      itemCount: products.length,
      // Defines how every grid item/cell should be build
      // Receives context and the index of the item it is currently building
      // a cell for
      // itemBuilder defines how the grid should be structured and gets a
      // GridView object from ProductItem with the details for every
      // loadedProducts item
      // ChangeNotifierProvider.value() is used when we use a provider on
      // something that is part of a list or a grid, value() makes sure that
      // the provider works even if data changes for the widget
      // value() should be used when we are providing data on single list or
      // grid
      // items where Flutter will recycle the widgets we are attaching the
      // provider to
      itemBuilder: (ctx, i) => ChangeNotifierProvider.value(
        // The builder provides a product for the ProductItem
        // builder: (c) => products[i],
        // When we reuse an existing object like here .value() is recommended
        value: products[i],
        child: ProductItem(
          // products[i].id,
          // products[i].title,
          // products[i].imageUrl,
        ),
      ),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        // The number of columns
        crossAxisCount: 2,
        childAspectRatio: 3 / 2,
        crossAxisSpacing: 10,
        // Space between the rows
        mainAxisSpacing: 10,
      ),
    );
  }
}
