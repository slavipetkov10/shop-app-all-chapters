import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../screens/product_detail_screen.dart';
import '../providers/product.dart';
import '../providers/cart.dart';
import '../providers/auth.dart';

// Grid Item rendered for every product
class ProductItem extends StatelessWidget {
  // final String id;
  // final String title;
  // final String imageUrl;
  //
  // ProductItem(this.id, this.title, this.imageUrl);

  @override
  Widget build(BuildContext context) {
    // Here we are interested in a single product. We get the nearest
    // provided product in the products grid, where a product is provided
    // for the ProductItem          builder: (c) => products[i],
    final product = Provider.of<Product>(context, listen: false);
    final cart = Provider.of<Cart>(context, listen: false);
    final authData = Provider.of<Auth>(context, listen: false);
    // Forces the child to wrap in a certain shape
    // with Consumer<Product>() we can wrap only a sub part of the widget
    // data that depends on the product data
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: GridTile(
        // Whenever we tap on an Image we go to a new screen
        child: GestureDetector(
          onTap: () {
            // Forward only the id to the named route, then extract the id from
            // the names route in the ProductDetailsScreen
            Navigator.of(context).pushNamed(
              ProductDetailScreen.routeName,
              arguments: product.id,
            );
          },
          // The Hero animation is used between two different screens, when a
          // product is opened it becomes bigger
          child: Hero(
            // Which image should animated to the new screen
            tag: product.id,
            // Adds fade in animation
            child: FadeInImage(
              placeholder: AssetImage('assets/images/product-placeholder.png'),
              //The image loaded once it is done loading
              image: NetworkImage(product.imageUrl),
              fit: BoxFit.cover,
            ),
          ),
        ),
        footer: GridTileBar(
          backgroundColor: Colors.black87,
          // The Consumer listens to changes in Product and rebuilds only the
          // IconButton nested child widget whenever the Product object changes
          // leading: Consumer<Product>(
          // builder: (ctx, product, child) => IconButton(
          // child is used when there is a child that we do not want
          // to rebuild, when we
          // rebuild the IconButton
          leading: Consumer<Product>(
            // child parameter is not used and it replaced with _
            builder: (ctx, product, _) => IconButton(
              icon: Icon(
                product.isFavorite ? Icons.favorite : Icons.favorite_border,
              ),
              color: Theme.of(context).accentColor,
              onPressed: () {
                product.toggleFavoriteStatus(
                  authData.token,
                  authData.userId,
                );
              },
            ),
          ),
          title: Text(
            product.title,
            textAlign: TextAlign.center,
          ),
          trailing: IconButton(
            icon: Icon(
              Icons.shopping_cart,
            ),
            onPressed: () {
              cart.addItem(product.id, product.price, product.title);
              Scaffold.of(context).hideCurrentSnackBar();
              // Establish a connection to the nearest scaffold widget, which
              // is in the products overview
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text(
                    'Added item to cart!',
                  ),
                  duration: Duration(seconds: 2),
                  action: SnackBarAction(
                    label: 'UNDO',
                    onPressed: () {
                      cart.removeSingleItem(product.id);
                    },
                  ),
                ),
              );
            },
            color: Theme.of(context).accentColor,
          ),
        ),
      ),
    );
  }
}
