import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/products.dart';

// Add a screen controlling th full page,
// It is showed after there is a tap on product
class ProductDetailScreen extends StatelessWidget {
  // final String title;
  // final double price;
  //
  // ProductDetailScreen(this.title, this.price);

  static const routeName = '/product-detail';

  @override
  Widget build(BuildContext context) {
    // Extract from the named route the id of the product, which was forwarded
    // from the product item
    final productId = ModalRoute.of(context).settings.arguments as String;
    // Find an item with specific id
    // With Provider.of<Products>(context) we setup a Products listener,
    // which reruns the build method when Products change
    // Load a single product based on the id and here I am not interested in
    // product's change
    final loadedProduct = Provider.of<Products>(
      context,
      // With listen: false this widget will not rebuild
      listen: false,
      // listen: false is used when we want to get data 1 time or dispatch
      // an action like addOrder where we are not interested in the result
    ).findById(productId);
    return Scaffold(
      // appBar: AppBar(
      //   title: Text(loadedProduct.title),
      // ),
      //Animate Image from the body into the AppBar
      body: CustomScrollView(
        // slivers are crowable areas on the screen
        slivers: <Widget>[
          SliverAppBar(
            // Height if it is not the AppBar, but the image
            expandedHeight: 300,
            // AppBar will always be visible
            pinned: true,
            //What is inside and how the content may change
            flexibleSpace: FlexibleSpaceBar(
              title: Text(loadedProduct.title),
              // Should be visible if the AppBar is expanded
              background: Hero(
                // The tag should be equal to the tag set in the product_item
                tag: productId,
                child: Image.network(
                  loadedProduct.imageUrl,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          // List view as a part of multiple slivers
          SliverList(
            // Tells it how to render the content of the list
            delegate: SliverChildListDelegate(
              [
                SizedBox(height: 10),
                Text(
                  '\$${loadedProduct.price}',
                  style: TextStyle(
                    color: Colors.grey,
                    fontSize: 20,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  width: double.infinity,
                  child: Text(
                    loadedProduct.description,
                    textAlign: TextAlign.center,
                    softWrap: true,
                  ),
                ),
                SizedBox(height: 800,),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
